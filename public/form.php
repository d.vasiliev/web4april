<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css" media="all">
    <title>Form</title>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
.error-radio{
  border: 2px solid red;
  width: 200px;
        }
.error-checkbox {
  width: 400px;
  border: 2px solid red;     
        }
    </style>
</head>
<body>

<?php
if (!empty($messages)) {
    print('<div id="messages">');
// Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}
?>    
    <div id="form">
    <form action="index.php" method="POST" id="form" accept-charset="UTF-8">
        <h1>Пожалуйста, введите Ваши данные!</h1>
    <label>
      Имя:
      <input type="text" name="fio" 
      <?php if ($errors['fio']) {print 'class="error"';} ?>
       value="<?php print $values['fio']; ?>"> <br>
    </label>
    <label>
     Электронная почта: <input type="email" name="email" 
      <?php if ($errors['email']) {print 'class="error"';} ?> 
      value="<?php print $values['email']; ?>"> <br>
    </label>
    <label>
      Дата рождения: 
      <input type="date" name="bdate" 
      <?php if($errors['bdate']) {print 'class = "error"';}?> 
      value="<?php print $values['bdate']; ?>" > <br>  
    </label>
    <label>
    Пол:
    <label <?php if($errors['sex']) { print 'class = "error-radio"';} ?>>
        <input type="radio" name="radio2" value="male"
        <?php if($values['sex']=="male") {print 'checked';}?>/>
        Мужчина</label>
    <label <?php if($errors['sex']) { print 'class = "error-radio"';} ?>>
        <input type="radio" name="radio2" value="female"
            <?php if($values['sex']=="female") {print 'checked';}?>/>
        Женщина</label>
    <br/>
    Количество имеющихся конечностей:
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="1"
            <?php if($values['limbs']=="1") {print 'checked';}?>/>
        1</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="2"
            <?php if($values['limbs']=="2") {print 'checked';}?>/>
        2</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="3"
            <?php if($values['limbs']=="3") {print 'checked';}?>/>
        3</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="4"
            <?php if($values['limbs']=="4") {print 'checked';}?>/>
        4</label>
    <label <?php if($errors['limbs']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="5"
            <?php if($values['limbs']=="5") {print 'checked';}?>/>
        5</label><br>
    <label>
      Сверхспособности: <br>
      <select name="superpower[]" id="superpowers" multiple="multiple">
        <option value="1">Бессмертие</option>
        <option value="1">Прохождение сквозь стены</option>
        <option value="1">Левитация </option>
      </select>
    </label>
    <br>

    <label>
        Биография: <br/>
    <textarea name="bio"
        <?php if ($errors['bio']) 
        {print 'class="error"';} ?>
    >
        </textarea>
    </label>
    <br/>
<!-- чекбокс-->
    <label <?php if($errors['check']) { print 'class = "error-checkbox"';} ?>>
       <input type="checkbox" checked="checked" name="checkbox" value="1"
       <?php if($values['check']=="1") {print 'checked';}?>>
       
       c <a href="ftp://random.com/programs/contract.pdf"> условиями контракта </a> ознакомлен(а).
    </label>
<!-- подтверждение и отправка формы -->
    <br>
    <br>
    <input type="submit" value="Отправить!" id="submit"/>
    </form>
</div>
</body>
